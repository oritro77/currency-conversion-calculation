import random


COUNTRY_WISE_CURRENCY = {
    "Lithunia": "EUR",
    "Latvia": "GBP",
    "Estonia": "EUR",
    "Bulgaria": "BGN",
    "Spain": "EUR",
    "Romania": "RON",
    "Poland": "PLN",
    "United Kingdom": "GBP",
    "Germany": "EUR",
    "Russia": "RUB",
    "Algeria": "DZD",
    "Albenia": "ALL",
    "Kosovo": "EUR",
    "Ukraine": "UAH",
    "Another country": "EUR",
}

EURO_CURRENCY_COUNTRY = {
    "Lithunia": "EUR",
    "Estonia": "EUR",
    "Spain": "EUR",
    "Germany": "EUR",
    "Kosovo": "EUR",
    "Another country": "EUR",
}

NON_EURO_CURRENCY_COUNTRY = {
    "Latvia": "GBP",
    "Bulgaria": "BGN",
    "Romania": "RON",
    "Poland": "PLN",
    "United Kingdom": "GBP",
    "Russia": "RUB",
    "Algeria": "DZD",
    "Albenia": "ALL",
    "Ukraine": "UAH",
}

def get_random_amount():
    return str(round(random.uniform(1, 100), 2))

def get_random_non_euro_currency_country():
    keys = [*NON_EURO_CURRENCY_COUNTRY]
    return keys[round(random.uniform(0, len(keys)-1))]

def get_random_euro_currency_country():
    keys = [*EURO_CURRENCY_COUNTRY]
    return keys[round(random.uniform(1, len(keys)))]
