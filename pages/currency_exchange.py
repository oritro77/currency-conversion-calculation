import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException, TimeoutException, NoSuchElementException

from helpers.common import *


class CurrencyExchange:
    URL = "https://www.paysera.bg/v2/en-LT/fees/currency-conversion-calculator"
    #URL = "https://www.paysera.com/v2/en-UA/fees/currency-conversion-calculator#/"
    HEADING = (By.CLASS_NAME, '.featured-heading')
    LOG_IN_BUTTON = (By.CSS_SELECTOR, '.featured-heading + div.row + a')
    SELL_AMOUNT_INPUT_FORM = (By.CSS_SELECTOR, '[data-ng-model="currencyExchangeVM.filter.from_amount"]')
    SELL_CURRENCY_SELECTOR = (By.CSS_SELECTOR, 'span[data-ng-transclude] > span')
    BUY_AMOUNT_INPUT_FORM = (By.CSS_SELECTOR, '[data-ng-model="currencyExchangeVM.filter.to_amount"]')
    BUY_CURRENCY_SELECTOR = (By.CSS_SELECTOR, '[data-ng-model="currencyExchangeVM.filter.to"]')
    FILTER_BUTTON = (By.XPATH, '//button[normalize-space()="Filter"]')
    CLEAR_FILTER_BUTTON = (By.XPATH, '//button[normalize-space()="Clear filter"]')
    LOCALIZATION_SELECTOR = (By.CSS_SELECTOR, '.localization-popover-toggle-footer > span')
    LOCALIZATION_COUNTY_SELECTOR = (By.CSS_SELECTOR, '#countries-dropdown')
    LOADER_GIF = (By.XPATH, '//img[contains(@src,"/v2/compiled/ajax-loader")]')
    LOCALIZATION_COUNTRIES = (By.CSS_SELECTOR, 'div[class="dropup open"] > ul[aria-labelledby="countries-dropdown"] > li > a')
    ALERT_MESSAGE = (By.CSS_SELECTOR, ".alert-danger")
    CLOSE_BUTTON = (By.CSS_SELECTOR, ".close")
    CURRENCY_TABLE_TH_TD_XP_QUERY = '//thead/tr[{}]/th[{}]'
    CURRENCY_TABLE_TB_TD_XP_QUERY = '//tbody/tr[{}]/td[{}]'
    CURRENCY_TABLE_TB_TR_XP_QUERY = '//tbody/tr'
    CURRENCY_TABLE_TB_TD_XP_QUERY = '//tbody/tr[1]/td'
    CURRENCY_TABLE_TB_TR_TD_TEXT_XP_QUERY = '//tbody/tr[{}]/td[{}][text()]'




    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def _wait_untill_element_is_located(self, element, delay=10):
        return WebDriverWait(self.browser, delay).until(EC.presence_of_element_located(element))

    def _wait_untill_text_is_present_in_form_value(self, element, delay=10):
        return WebDriverWait(self.browser, delay).until(EC.text_to_be_present_in_element_value(element))

    def _return_desired_localization_country(self, target):
        countries = self.browser.find_elements(*self.LOCALIZATION_COUNTRIES)
        for country in countries:
            if country.text == target:
                return country

    def is_currency_exchange_table_loaded(self, delay=10):
        try:
            element = WebDriverWait(self.browser, delay).until(EC.visibility_of_element_located(self.LOADER_GIF))
        except (WebDriverException, NoSuchElementException, TimeoutException) as e:
            print("Loader did not show up {}".format(e))
            if(WebDriverWait(self.browser, delay).until(EC.invisibility_of_element(self.LOADER_GIF))):
                return True
            else:
                return False
        else:
            if (WebDriverWait(self.browser, delay).until(EC.invisibility_of_element(element))):
                return True


    def get_buy_input_form_value(self):
        buy_input_form = self._wait_untill_element_is_located(self.BUY_AMOUNT_INPUT_FORM)
        return buy_input_form.get_attribute("value")

    def get_sell_input_form_value(self):
        sell_input_form = self._wait_untill_element_is_located(self.SELL_AMOUNT_INPUT_FORM)
        return sell_input_form.get_attribute("value")

    def write_in_buy_input_form(self, amount, press_enter=0):
        buy_input_form = self.browser.find_element(*self.BUY_AMOUNT_INPUT_FORM)
        buy_input_form.clear()
        if press_enter == 1:
            buy_input_form.send_keys(amount + Keys.ENTER)
        else:
            buy_input_form.send_keys(amount)


    def write_in_sell_input_form(self, amount, press_enter=0):
        sell_input_form = self.browser.find_element(*self.SELL_AMOUNT_INPUT_FORM)
        sell_input_form.clear()

        if press_enter == 1:
            sell_input_form.send_keys(amount + Keys.ENTER)
        else:
            sell_input_form.send_keys(amount)


    def get_country_for_localization(self, delay=10):
        try:
            localization_button = WebDriverWait(self.browser, delay).until(EC.visibility_of_element_located(self.LOCALIZATION_SELECTOR))
        except (WebDriverException, NoSuchElementException, TimeoutException) as e:
            print("localization button not found " + e)
        else:
            localization_button.click()

        try:
            localization_country_button = WebDriverWait(self.browser, delay).until(EC.visibility_of_element_located(self.LOCALIZATION_COUNTY_SELECTOR))
        except (WebDriverException, NoSuchElementException, TimeoutException) as e:
            print("localization country button not found " + e)
        localization_country_button.click()
        return localization_country_button.text

    def get_currency_sell(self):
        sell_currency_selector = self.browser.find_element(*self.SELL_CURRENCY_SELECTOR)
        return sell_currency_selector.text

    def select_country_for_localization(self, country):
        script = 'var el = document.querySelector(".js-localization-popover");\
                el.click();\
                el = document.querySelector("#countries-dropdown");\
                el.click();\
                els = document.querySelectorAll("#countries-dropdown + ul > li");\
                for(var i=0; i< els.length; i++){\
                    if(els[i].getElementsByTagName("a")[0].text.trim() === arguments[0]){\
                        els[i].getElementsByTagName("a")[0].click();\
                    }\
                }'

        self.browser.execute_script(script, country)


    def get_all_the_currency_table_values(self):
        row_len = len(self.browser.find_elements(By.XPATH, self.CURRENCY_TABLE_TB_TR_XP_QUERY))
        col_len = len(self.browser.find_elements(By.XPATH, self.CURRENCY_TABLE_TB_TD_XP_QUERY))
        tb_data = {}
        tb_data["data"] = []
        for i in range(1, row_len+1):
            temp = {}
            for j in range(1, col_len+1):
                td = self.browser.find_element(By.XPATH, self.CURRENCY_TABLE_TB_TR_TD_TEXT_XP_QUERY.format(i, j))
                if j == 1:
                    temp["currency"] = None if td.text == "-" else td.text[:3]
                if j == 2:
                    temp["official_rate"] = return_none_value_hyphen(td.text)
                if j == 3:
                    temp["rate"] = return_none_value_hyphen(td.text)
                if j == 4:
                    temp["amount"] = return_none_value_hyphen(td.text)

                if "\n" in td.text:
                    temp_val = td.text.split("\n")
                    loss = temp_val[1].strip("()")
                    temp[td.get_attribute("data-title")] = {"amount": temp_val[0], "loss": loss}

                if j > 4 and td.get_attribute("data-title") is not None and "\n" not in td.text:
                    temp[td.get_attribute("data-title")] = {"amount": return_none_value_hyphen(td.text)}
            tb_data["data"].append(temp)

        return tb_data

    def is_error_message_visible(self):
        try:
            message = self.browser.find_element(*self.ALERT_MESSAGE)
        except NoSuchElementException as e:
            print(e)
            return False
        else:
            return message.is_displayed()

    def click_on_error_message_cross_button(self):
        cross_button = self.browser.find_element(*self.CLOSE_BUTTON)
        cross_button.click()