import pytest

from pages.currency_exchange import CurrencyExchange
from data.random_data import *
from helpers.common import *
from custom_exception.currency_conversion_calculation_exception \
    import UserLossNotShownError, CurrencyRateNotUpdatedError

# TODO Implement Logger

def test_user_fills_buy_amount_sell_amount_form_gets_emptied_and_vice_verca(chrome_browser):
    # step 1: load the page
    currency_exchange_page = CurrencyExchange(chrome_browser)
    currency_exchange_page.load()
    if currency_exchange_page.is_currency_exchange_table_loaded():
        # step 2: write in sell_amount
        amount = get_random_amount()
        currency_exchange_page.write_in_sell_input_form(amount)
        sell_form_val = currency_exchange_page.get_sell_input_form_value()
        assert sell_form_val == amount

        # step 3: write in buy amount
        currency_exchange_page.write_in_buy_input_form(amount)
        buy_form_val = currency_exchange_page.get_buy_input_form_value()
        assert buy_form_val == amount

        # test sell amount is emptied
        sell_form_val = currency_exchange_page.get_sell_input_form_value()
        assert sell_form_val == ""

        # step 4: write in sell amount
        currency_exchange_page.write_in_sell_input_form(amount)

        # test buy amount is emptied
        buy_form_val = currency_exchange_page.get_buy_input_form_value()
        assert buy_form_val == ""


def test_user_selects_different_country_currency_option_changes_to_the_country_currency(chrome_browser):
    # step 1: page loads
    currency_exchange_page = CurrencyExchange(chrome_browser)
    currency_exchange_page.load()
    if currency_exchange_page.is_currency_exchange_table_loaded():

        # step 2: check for default country and currency
        default_country = currency_exchange_page.get_country_for_localization()
        assert default_country == "Lithuania"
        default_currency_sell = currency_exchange_page.get_currency_sell()
        assert default_currency_sell == "EUR"

        # step 3: now selects a random country which has different currency other than euro
        country_to_test = get_random_non_euro_currency_country()
        currency_exchange_page.select_country_for_localization(country=country_to_test)

        if currency_exchange_page.is_currency_exchange_table_loaded():
            # step 5: check the default currency is the currency of the selected country
            changed_country = currency_exchange_page.get_country_for_localization()

            assert changed_country == country_to_test
            changed_currency_sell = currency_exchange_page.get_currency_sell()

            assert changed_currency_sell == COUNTRY_WISE_CURRENCY[country_to_test]


def test_user_selects_different_country_currency_rate_updates_not_euro(chrome_browser):
    # 1 load the page
    currency_exchange_page = CurrencyExchange(chrome_browser)
    currency_exchange_page.load()
    default_country = "Lithuania"
    # next_country = "Romania"
    next_country = get_random_non_euro_currency_country()
    error_message = "{} table {} collumn for currency {} has value {} " \
                    "and {} table {} collumn for currency {} has value {} which are same."
    errors = []

    if currency_exchange_page.is_currency_exchange_table_loaded():
        # 2 get the default table data
        current_tbl_val = currency_exchange_page.get_all_the_currency_table_values()
    # 3 go to the next country table
    currency_exchange_page.select_country_for_localization(country=next_country)
    if currency_exchange_page.is_currency_exchange_table_loaded():
        # 5 get the data for the next table
        next_tbl_val = currency_exchange_page.get_all_the_currency_table_values()

    assert len(current_tbl_val) == len(next_tbl_val)
    current_tbl_val = current_tbl_val["data"]
    next_tbl_val = next_tbl_val["data"]
    # 6 compare the data and see if the rate updates
    for i in range(len(current_tbl_val)):
        is_currency_rate_update = True
        keys1 = current_tbl_val[i].keys()
        keys2 = next_tbl_val[i].keys()
        # for tables which has equal number of collumns
        same_length = False
        if len(keys1) == len(keys2):
            same_length = True

        if not same_length:
            # for tables which has not equal number of collumns, getting the same collumns
            common = list(set(keys1).intersection(keys2))
            keys1 = common
            keys2 = common

        for x, y in zip(keys1, keys2):
            if is_currency(x) and is_currency(y):
                break
            is_dict = False
            if type(current_tbl_val[i][x]) is dict and type(next_tbl_val[i][y]) is dict:
                is_dict = True

            if is_dict:
                if check_equal_and_not_none(current_tbl_val[i][x]["amount"],
                                            next_tbl_val[i][y]["amount"]):
                    is_currency_rate_update = False

            if check_equal_and_not_none(current_tbl_val[i][x], next_tbl_val[i][y]):
                    is_currency_rate_update = False

            if is_currency_rate_update is False:
                errors.append(error_message.format(default_country,
                                                   x,
                                                   current_tbl_val[i]["currency"],
                                                   current_tbl_val[i][x]["amount"],
                                                   next_country,
                                                   y,
                                                   next_tbl_val[i]["currency"],
                                                   next_tbl_val[i][y]["amount"]))

        if len(errors) != 0:
            error = '\n'.join([str(e) for e in errors])
            raise CurrencyRateNotUpdatedError(error)



def test_shows_loss_to_user_for_amount_and_provider_exchange_amount(chrome_browser):
    country = "Lithuania"
    errors = []
    loss_not_shown_message = "In currency exchange tables for country {}, the amount {} is" \
                             " greater then bank {} amount {} for currency {} but loss is not shown"
    loss_calculation_is_wrong_message = "In currency exchange tables for {}, the difference between" \
                                        " amount {} and bank {} amount {} for currency {} is {} " \
                                        "but loss is shown as {}"

    # 1 load the page
    currency_exchange_page = CurrencyExchange(chrome_browser)
    currency_exchange_page.load()
    if currency_exchange_page.is_currency_exchange_table_loaded():
        # 2 get the default table data
        current_tbl_val = currency_exchange_page.get_all_the_currency_table_values()
        current_tbl_val = current_tbl_val["data"]
        row = current_tbl_val[1]
        keys = row.keys()
        # if length of collumn is less then 4 then comparison will not shown currently
        if len(keys) > 4:
            for i in range(len(current_tbl_val)):
                row = current_tbl_val[i]
                keys = [*row]
                keys_to_check = keys[4:]
                for k in keys_to_check:
                    if row[k]["amount"] is None:
                        break
                    amount = convert_into_float(row["amount"])
                    other_bank_amount = convert_into_float(row[k]["amount"])
                    if (amount > other_bank_amount):
                        if "loss" not in row[k]:
                            errors.append(loss_not_shown_message.format(country,
                                                                        amount,
                                                                        k,
                                                                        other_bank_amount,
                                                                        row["currency"]))
                            break
                        # loss = convert_into_float(row[k]["loss"])
                        # calculated_loss = round(other_bank_amount - amount, 2)
                        # if ( calculated_loss != loss):
                        #     errors.append(loss_calculation_is_wrong_message.format(country,
                        #                                                            amount,
                        #                                                            k,
                        #                                                            other_bank_amount,
                        #                                                            row["currency"],
                        #                                                            calculated_loss,
                        #                                                            loss))

    if (len(errors) > 0):
        error = '\n'.join([str(e) for e in errors])
        raise UserLossNotShownError(error)


@pytest.mark.skip
def test_clear_filter():
    # TODO
    pass


@pytest.mark.skip
def test_filter_option():
    # TODO
    pass


def test_non_numeric_input_for_sell_and_buy_shows_error(chrome_browser):
    currency_exchange_page = CurrencyExchange(chrome_browser)
    currency_exchange_page.load()
    if currency_exchange_page.is_currency_exchange_table_loaded():
        # 1. write alpha numeric in sell form and press enter
        currency_exchange_page.write_in_sell_input_form(amount="1234aerqwe324vcxb%@!@", press_enter=1)
        if currency_exchange_page.is_currency_exchange_table_loaded():
            # 2. check table is empty
            tbl = currency_exchange_page.get_all_the_currency_table_values()
            assert is_table_empty(tbl) is True
            # 3. check sell input form is empty
            sell_form_val = currency_exchange_page.get_sell_input_form_value()
            assert sell_form_val == ""
            # 4. check error message visible
            assert currency_exchange_page.is_error_message_visible() is True
            # 5 click on cross button of error message
            currency_exchange_page.click_on_error_message_cross_button()
            # 6. check if error message gone
            assert currency_exchange_page.is_error_message_visible() is False
            # 7. same for buy input form
            currency_exchange_page.write_in_buy_input_form(amount="1234aerqwe324vcxb%@!@", press_enter=1)
            if currency_exchange_page.is_currency_exchange_table_loaded():
                assert is_table_empty(tbl) is True
                buy_form_val = currency_exchange_page.get_buy_input_form_value()
                assert buy_form_val == ""
                assert currency_exchange_page.is_error_message_visible() is True
                currency_exchange_page.click_on_error_message_cross_button()
                assert currency_exchange_page.is_error_message_visible() is False
