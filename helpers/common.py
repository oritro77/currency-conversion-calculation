def is_currency(x):
    return x == "currency"


def check_equal_and_not_none(x, y):
    if x == y and (x is not None or y is not None):
        return True
    return False


def convert_into_float(amount):
    return float(amount.replace(",", ""))


def return_none_value_hyphen(x):
    return None if x.strip() == "-" or x.strip() == "" else x


def is_table_empty(tbl):
    tbl_data = tbl["data"]
    for i in range(len(tbl_data)):
        row = tbl_data[i]
        keys = [*row]
        for k in keys:
            if row[k] is not None and type(row[k]) is not dict:
                return False
            elif type(row[k]) is dict and row[k]["amount"] is not None:
                return False
    return True
