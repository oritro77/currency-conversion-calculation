class Error(Exception):
    """Base class"""
    pass

class UserLossNotShownError(Error):
    """Exception showed when loss is not shown in currency conversion calculation"""

    def __init__(self, message):
        self.message = message

class WrongLossCalculationError(Error):
    """Exception showed when loss is not showed but the calculation is wrong
    in currency conversion calculation"""

    def __init__(self, message):
        self.message = message

class CurrencyRateNotUpdatedError(Error):
    """Exception showed when localiztion country is changed but
    currency rates are not updated currency conversion calculation"""

    def __init__(self, message):
        self.message = message