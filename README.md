# Currency Conversion Calculatio test with selenium and pytest

Coded with Python 3.8. To run the scripts do the following:
1. Download chrome [webdriver](https://chromedriver.chromium.org/downloads) and [geckodriver](https://github.com/mozilla/geckodriver/releases)
1. Put them in the system path
1. In the project folder run ```pip install requirements.txt``` 
1. Then run ```python -m pytest -p no:cacheprovider -s```
